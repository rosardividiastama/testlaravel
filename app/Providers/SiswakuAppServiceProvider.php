<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Request;

class SiswakuAppServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // definisi variabel $halaman
        // nilai dri url ke segment, ke siswa, lalu ke $halaman

        $halaman = '';
        if (Request::segment(1) == 'siswa') {
            # code...
            $halaman = 'siswa';
        }

        if (Request::segment(1) == 'about') {
            # code...
            $halaman = 'about';
        }
        view()->share('halaman', $halaman);
    }
}
