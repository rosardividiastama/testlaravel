<?php

namespace App\Http\Controllers;

use App\Siswa;
use Illuminate\Http\Request;

class SiswaController extends Controller
{
    //
    public function index(){
        //$halaman = 'siswa';
        $siswa_list= Siswa::all()->sortBy('nama_siswa');
        $jumlah_siswa= $siswa_list->count();
        return view('siswa.index', compact('halaman','siswa_list','jumlah_siswa'));
    }

    public function create(){
        //$halaman = 'siswa';
        return view('siswa.create', compact('halaman'));
    }

    // public function store(Request $request){
    //     $siswa = new Siswa;
    //     $siswa->nisn = $request->nisn;
    //     $siswa->nama_siswa = $request->nama_siswa;
    //     $siswa->tanggal_lahir = $request->tanggal_lahir;
    //     $siswa->jenis_kelamin = $request->jenis_kelamin;
    //     $siswa->save();
    //     return redirect('siswa');
    // }

    public function store(Request $request){
        Siswa::create($request->all());
        return redirect('siswa');
    }

    public function edit($id){
        $siswa = Siswa::findOrFail($id);
        return view ('siswa.edit', compact('siswa'));
    }

    public function update ($id, Request $request){
        $siswa= Siswa::findOrFail($id);
        $siswa->update($request->all());
        return redirect('siswa');
    }

    public function destroy($id){
        $siswa = Siswa::findOrFail($id);
        $siswa->delete();
        return redirect('siswa');
    }
}
