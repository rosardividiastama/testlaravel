<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RahasiaController extends Controller
{
    //
    public function halamanRahasia(){
        return 'anda sedang melihat halaman rahasia';
    }

    // public function showMeSecret(){
    //     return redirect()->route('secret');
    // }

    public function showMeSecret(){
        $url = route('secret');
        $link = '<a href="'.$url.'"> ke halaman rahasia</a>';
        return $link;
    }
}
