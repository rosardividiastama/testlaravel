<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@homepage');

Route::get('about', 'PagesController@about');

Route::get('siswa', 'SiswaController@index');

// Route::get('halaman-rahasia',[
//     'as' => 'secret',
//     'uses' => 'RahasiaController@halamanRahasia'
// ]);

Route::get('halaman-rahasia', 'RahasiaController@showMeSecret')
->name('secret');

Route::get('showmesecret', 'RahasiaController@showMeSecret');

Route::get('siswa/create', 'SiswaController@create');

Route::post('siswa', 'SiswaController@store');

Route::get('siswa/{siswa}/edit', 'SiswaController@edit');

Route::patch('siswa/{siswa}', 'SiswaController@update');

Route::delete('siswa/{siswa}', 'SiswaController@destroy');

// Route::get('sampledata', function () {
//     DB::table('siswa')->insert([
//     [
//         'nisn' => '1001',
//         'nama_siswa' => 'Agus',
//         'tanggal_lahir' => '1990-02-12',
//         'jenis_kelamin' => 'L',
//         'created_at' => '2016-03-10 19:10:15',
//         'updated_at' => '2016-03-10 19:10:15'
//     ],
//     [
//         'nisn' => '1002',
//         'nama_siswa' => 'Meli',
//         'tanggal_lahir' => '1990-02-12',
//         'jenis_kelamin' => 'L',
//         'created_at' => '2016-03-10 19:10:15',
//         'updated_at' => '2016-03-10 19:10:15'
//     ],
//     [
//         'nisn' => '1003',
//         'nama_siswa' => 'Tina',
//         'tanggal_lahir' => '1990-02-12',
//         'jenis_kelamin' => 'P',
//         'created_at' => '2016-03-10 19:10:15',
//         'updated_at' => '2016-03-10 19:10:15'
//     ],
//     [
//         'nisn' => '1004',
//         'nama_siswa' => 'Timi',
//         'tanggal_lahir' => '1990-02-12',
//         'jenis_kelamin' => 'L',
//         'created_at' => '2016-03-10 19:10:15',
//         'updated_at' => '2016-03-10 19:10:15'
//     ],
// ]);
// });




