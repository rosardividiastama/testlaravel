@extends('template')
@section('main')
    {{-- blade1 --}}
    
    <div id="siswa">
        <h2>siswa</h2>
        
        @if (!empty($siswa_list))
            <table class='table'>
                <thead>
                    <tr>
                        <th>NISN</th>
                        <th>Nama</th>
                        <th>Tgl Lahir</th>
                        <th>JK</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($siswa_list as $siswa)
                        <tr>
                            <td>{{ $siswa->nisn}}</td>
                            <td>{{ $siswa->nama_siswa}}</td>
                            <td>{{ $siswa->tanggal_lahir}}</td>
                            <td>{{ $siswa->jenis_kelamin}}</td>
                            <td>
                                {{ link_to('siswa/'. $siswa->id. '/edit', 'Edit', ['class'=>'btn btn-warning btn-sm']) }}
                            </td>
                            <td>
                                {!! Form::open(['method'=>'DELETE', 'action' => ['SiswaController@destroy', $siswa->id]]) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            
        @else
            <p>Tidak ada data siswa.</p>
        @endif

        {{-- count dari jumlah siswa --}}
        <div class="table-bottom">
            <div class="text-left">
                <strong>Jumlah Siswa : {{ $jumlah_siswa }}</strong>
            </div>
            <div class="text-right">
                Pagination
            </div>
        </div>

        {{-- tambah siswa --}}
        <div class="bottom-nav">
            <div>
                <a href="siswa/create" class="btn btn-primary">
                Tambah Siswa</a>
            </div>
        </div>

    </div>
@endsection

@section('footer')
<div id="footer">
    <p>&copy;2019 Siswaku App</p>
</div>
@endsection