@extends('template')
@section('main')
    <div id="siswa">
        <h2>Tambah Siswa</h2>
        
        {{-- <form action="{{url('siswa')}}" method="POST"> --}}
        {!! Form::open(['url'=> 'siswa'])!!}
            <div class="form-group">
                    {!! Form::label('nisn','NISN:', ['class'=>'control-label']) !!}
                    {!! Form::text('nisn', null, ['class'=>'form-control']) !!}
                {{-- <label for="nisn" class="control-label">NISN</label>
                <input type="text" name="nisn" id="nisn" class="form-control"> --}}
            </div>

            <div class="form-group">
                    {!! Form::label('nama_siswa','Nama:', ['class'=>'control-label']) !!}
                    {!! Form::text('nama_siswa', null, ['class'=>'form-control']) !!}
                {{-- <label for="nama_siswa" class="control_label">Nama</label>
                <input type="text" name="nama_siswa" id="nama_siswa" class="form-control"> --}}
            </div>

            <div class="form-group">
                    {!! Form::label('tanggal_lahir','Tanggal Lahir:', ['class'=>'control-label']) !!}
                    {!! Form::date('tanggal_lahir', null, ['class'=>'form-control', 'id'=>'tanggal_lahir']) !!}
                {{-- <label for="tanggal_lahir" class="control_label">Tgl Lahir</label>
                <input type="text" name="tanggal_lahir" id="tanggal_lahir" class="form-control"> --}}
            </div>

            <div class="form-group">
                    {!! Form::label('jenis_kelamin','Jenis Kelamin:', ['class'=>'control-label']) !!}
                {{-- <label for="jenis_kelamin" class="control_label">Jenis Kelamin</label> --}}
                    <div class="radio">
                        <label>{!! Form::radio('jenis_kelamin','L') !!}
                        Laki-laki</label>
                        {{-- <input type="radio" name="jenis_kelamin" id="jenis_kelamin" value="L"> --}}
                            
                    </div>
                    <div class="radio">
                        <label>{!! Form::radio('jenis_kelamin','P') !!}
                        Perempuan</label>
                        {{-- <input type="radio" name="jenis_kelamin" id="jenis_kelamin" value="P"> --}}
                            
                    </div>
            </div>

            <div class="form-group">
                    {!! Form::submit('Tambah Siswa', ['class'=>'btn btn-primary form-control']) !!}
                {{-- <input type="submit" class="btn btn-primary form-control" value="Simpan" > --}}
            </div>
            {!! Form::close()!!}
    </div>
@endsection

@section('footer')
    @include('footer')
@endsection