@extends('template')
@section('main')
    {{-- blade1 --}}
    
    <div id="siswa">
        <h2>siswa</h2>
        
        <?php if (!empty($siswa)):?>
        <ul>
            <?php foreach ($siswa as $anak):?>
            <li><?= $anak ?></li>
            <?php endforeach ?>
        </ul>
        <?php else: ?>
            <p>TIdak ada data</p>
        <?php endif ?>
    </div>

    {{-- blade2 --}}

    <div id="siswa">
        <h2>siswa</h2>

    @if (!empty($siswa))
    <ul>
        <?php foreach ($siswa as $anak):?>
            <li><?= $anak ?></li>
        <?php endforeach ?>
    </ul>
    @else
        <p>TIdak ada data</p>
    @endif
    </div>

    {{-- blade3 --}}
    <div id="siswa">
        <h2>siswa</h2>

    @if (!empty($siswa))
        @foreach ($siswa as $anak)
            <li>{{$anak}}</li>
        @endforeach
    @else
        <p>TIdak ada data</p>
    @endif
    </div>
@endsection

@section('footer')
<div id="footer">
    <p>&copy;2019</p>
</div>
@endsection