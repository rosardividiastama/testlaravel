@extends('template')
@section('main')
    <div id="siswa">
        <h2>tambah siswa</h2>
        <form action="{{url('siswa')}}" method="POST">
            @csrf

            <div class="form-group">
                <label for="nisn" class="control-label">NISN</label>
                <input type="text" name="nisn" id="nisn" class="form-control">
            </div>

            <div class="form-group">
                <label for="nama_siswa" class="control_label">Nama</label>
                <input type="text" name="nama_siswa" id="nama_siswa" class="form-control">
            </div>

            <div class="form-group">
                <label for="tanggal_lahir" class="control_label">Tgl Lahir</label>
                <input type="text" name="tanggal_lahir" id="tanggal_lahir" class="form-control">
            </div>

            <div class="form-group">
                <label for="jenis_kelamin" class="control_label">Jenis Kelamin</label>
                    <div class="radio">
                        <label><input type="radio" name="jenis_kelamin" id="jenis_kelamin" value="L">
                        Laki-laki</label>
                    </div>
                    <div class="radio">
                        <label><input type="radio" name="jenis_kelamin" id="jenis_kelamin" value="P">
                        Perempuan</label>
                        </div>
            </div>

            <div class="form-group">
                <input type="submit" class="btn btn-primary form-control" value="Simpan" >
            </div>

        </form>

    </div>
@endsection

@section('footer')
    <div id="footer">
        <p>&copy; 2019 SiswakuApp</p>
    </div>
@endsection