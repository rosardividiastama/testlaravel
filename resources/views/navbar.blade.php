<nav class="navbar navbar-dark">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapse" 
                data-toggle="collapse"
                data-target="#bs-example-navbar-collapse-1"
                aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        <a class="navbar-brand" href="{{ url('/')}}">LaravelApp</a>
        </div>

        {{-- navbar-collapse --}}
        <div>
            <ul class="nav navbar-nav">
                {{-- navbar efek terpilih --}}
                @if (!empty($halaman) && $halaman == 'siswa')
                    <li class="active"><a href="{{ url('siswa')}}">Siswa 
                        <span class="sr-only">(current)</span></a>
                    </li>
                @else
                    <li><a href="{{ url('siswa')}}">Siswa2</a></li>
                @endif

                @if (!empty($halaman) && $halaman == 'about')
                    <li class="active"><a href="{{ url('about')}}">About 
                        <span class="sr-only">(current)</span></a>
                    </li>
                @else
                    <li><a href="{{ url('about')}}">About2</a></li>
                @endif

                {{-- <li><a href="{{ url('siswa')}}">Siswa</a></li>
                <li><a href="{{ url('about')}}">About</a></li> --}}
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#">Login</a></li>
                <li class="dropdown"></li>
            </ul>
        </div>
    </div>
</nav>